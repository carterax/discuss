defmodule Discuss.Repo.Migrations.AddTimestampsToTopics do
  use Ecto.Migration

  def change do
    alter table(:topics) do
      timestamps null: true
    end
  end
end
