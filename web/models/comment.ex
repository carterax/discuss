defmodule Discuss.Comment do
  use Discuss.Web, :model

  # Poison.Encoder the http json parser for phoenix
  # instruct phoenix to handle only the content field when parsed over sockets
  # to json on the client side, because phoenix cannot handle dates as json
  @derive {Poison.Encoder, only: [:content, :user]}

  schema "comments" do
    field :content, :string
    belongs_to :user, Discuss.User
    belongs_to :topic, Discuss.Topic

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:content])
    |> validate_required([:content])
  end
end
