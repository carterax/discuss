defmodule Discuss.TopicController do
  use Discuss.Web, :controller

  # shortcut to Discuss.Topic Module
  alias Discuss.Topic

  # require module auth plug for given controller actions
  plug Discuss.Plugs.RequireAuth when action in [:new, :create, :edit, :update, :delete]

  # check topic owner function plug
  plug :check_topic_owner when action in [:edit, :update, :delete]

  def index(conn, _params) do
    topics = Repo.all(Topic)
    render conn, "index.html", topics: topics
  end

  def show(conn, params) do
    topic = Repo.get!(Topic, params["id"])
    render conn, "show.html", topic: topic
  end

  def new(conn, _params) do
    changeset = Topic.changeset(%Topic{}, %{})

    render conn, "new.html", changeset: changeset
  end

  def create(conn, %{"topic" => topic}) do
    # changeset = Topic.changeset(%Topic{}, topic)

    changeset = conn.assigns.user
    |> build_assoc(:topics)
    |> Topic.changeset(topic)

    case Repo.insert(changeset) do
      {:ok, _topic} ->
        conn
        |> put_flash(:info, "Topic created")
        |> redirect(to: topic_path(conn, :index))
      {:error, changeset} ->
        render conn, "new.html", changeset: changeset
    end
  end

  def edit(conn, %{"id" => topic_id}) do
    # get the topic by ID from postgres
    topic = Repo.get(Topic, topic_id)

    # create a changeset we can pass to our forms
    changeset = Topic.changeset(topic)

    # pass to forms during render as changeset
    render conn, "edit.html", changeset: changeset, topic: topic
  end

  def update(conn, %{"id" => topic_id, "topic" => topic}) do
    # old_topic = Repo.get(Topic, topic_id)
    # changeset = Topic.changeset(old_topic, topic)
    old_topic = Repo.get(Topic, topic_id)
    changeset = Topic.changeset(old_topic, topic)

    case Repo.update(changeset) do
      {:ok, _topic} ->
        conn
        |> put_flash(:info, "Topic updated")
        |> redirect(to: topic_path(conn, :index))
      {:error, changeset} ->
        render conn, "edit.html", changeset: changeset, topic: old_topic
    end
  end

  def delete(conn, %{"id" => topic_id}) do
    Repo.get!(Topic, topic_id) |> Repo.delete!

    conn
    |> put_flash(:info, "Topic deleted")
    |> redirect(to: topic_path(conn, :index))
  end

  def check_topic_owner(%{params: %{"id" => topic_id}} = conn, _params) do
    if conn.assigns.user.id == Repo.get(Topic, topic_id).user_id do
      conn
    else
      conn
      |> put_flash(:error, "Cannot edit the topic!")
      |> redirect(to: topic_path(conn, :index))
      |> halt()
    end
  end
end
