defmodule Discuss.CommentsChannel do
  use Discuss.Web, :channel
  alias Discuss.{Topic, Comment}

  def join("comments:" <> topic_id, _params, socket) do
    topic_id = String.to_integer(topic_id)

    # fetch associated data using Repo.preload
    # fetch comments that belong to the current topic
    # fetch comments that belong to current user and pass to frontend
    topic = Topic
    |> Repo.get(topic_id)
    |> Repo.preload(comments: [:user])

    {:ok, %{comments: topic.comments}, assign(socket, :topic, topic)}
  end

  # name - name of event used in client side
  # message - object sent from client
  # socket -  lifecycle representation of our socket connection (headers)
  # must return a reponse
  def handle_in(name, %{"content" => content}, socket) do
    topic = socket.assigns.topic
    user_id = socket.assigns.user_id

    changeset = topic
    |> build_assoc(:comments, user_id: user_id)
    |> Comment.changeset(%{content: content})

    case Repo.insert(changeset) do
      {:ok, comment} ->
        # after receiving and inserting the topic send out a broadcast to the client
        broadcast!(socket, "comment:#{socket.assigns.topic.id}:new", %{comment: comment})
        {:reply, :ok, socket}
      {:error, _reason} ->
        {:reply, {:error, %{error: changeset}}, socket}
    end
  end
end
